import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import TasksPage from '@/components/TasksPage'
import LoginPage from '@/components/LoginPage'
import SignUpPage from '@/components/SignUpPage'
import ProfilePage from '@/components/ProfilePage'
import HomePage from '@/components/HomePage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/tasks',
      name: 'TasksPage',
      component: TasksPage
    },
    {
      path: '/login',
      name: 'LoginPage',
      component: LoginPage
    },
    {
      path: '/signup',
      name: 'SignUpPage',
      component: SignUpPage
    },
    {
      path: '/profile',
      name: 'ProfilePage',
      component: ProfilePage
    },
    {
      path: '/home',
      name: 'HomePage',
      component: HomePage
    }
  ]
})
